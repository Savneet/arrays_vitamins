const items = require("./3-arrays-vitamins.cjs");

// 5. Sort items based on number of Vitamins they contain.

noOfVitamins = {};

items.forEach(function (item) {
  noOfVitamins[item.name] = item.contains.split(",").length;
});

let sortedItem= Object.entries(noOfVitamins).sort(([,a],[,b])=>b-a);
// console.log(Object.entries(noOfVitamins).sort(([, a], [, b]) => b - a));

sortedItem.forEach(function(element)
{
    console.log(element);

});

console.log('Sorting items based on no of vitamins:');
sortedItem.forEach(function(element)
{
    console.log(items.find((element1)=>element1.name==element[0]));
})