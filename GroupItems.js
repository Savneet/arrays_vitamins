const items = require('./3-arrays-vitamins.cjs');

// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

// and so on for all items and all Vitamins.node 

const groupItems={};

items.forEach(function(item)
{
    item.contains=item.contains.split(",");
    item.contains.forEach(function(element)
    {
        element=element.trim();
        if(!groupItems[element])
        {
            groupItems[element]=[];
            groupItems[element].push(item.name);
        }
        else{
            groupItems[element].push(item.name);
        }
    })
})
console.log(groupItems);